import java.util.Scanner;

public class Calculadora_main {

	public static void main(String[] args) {
		
		Calculadora_operaciones calculadora = new Calculadora_operaciones();
		
		int opcion, x, y;
		Scanner in = new Scanner(System.in);
		
		do {
			System.out.println("  Calculadora  ");
			System.out.println("Opcion 1: Suma");
			System.out.println("Opcion 2: Resta");
			System.out.println("Opcion 3: Multiplicacion");
			System.out.println("Opcion 4: Divicion");
			System.out.println("Seleciona una opcion: ");
			opcion = in.nextInt();
			
	        switch (opcion) {
	            case 1:	System.out.println("Suma");
	            		System.out.println("Ingresa el primer numero: ");
	            		x = in.nextInt();
	            		System.out.println("Ingresa el segundo numero: ");
	            		y = in.nextInt();
	            		System.out.println(calculadora.suma(x, y));
	                    break;
	            case 2:	System.out.println("Resta");
	            		System.out.println("Ingresa el primer numero: ");
		        		x = in.nextInt();
		        		System.out.println("Ingresa el segundo numero: ");
		        		y = in.nextInt();
		        		System.out.println(calculadora.resta(x, y));
		                break;
	            case 3:	System.out.println("Multiplicacion");
	            		System.out.println("Ingresa el primer numero: ");		
	            		x = in.nextInt();
	            		System.out.println("Ingresa el segundo numero: ");
		        		y = in.nextInt();
		        		System.out.println(calculadora.multiplicacion(x, y));
		                break;
	            case 4:	System.out.println("Division");
	            		System.out.println("Ingresa el primer numero: ");
	            		x = in.nextInt();
	            		System.out.println("Ingresa el segundo numero: ");
		        		y = in.nextInt();
		        		System.out.println(calculadora.divicion(x, y));
		                break;
	            default: opcion = 5;
	                     break;
	        }	        
		} while (opcion != 5);
		
	}
}